export interface AccountModel {
    account_name: string;
    created_at: string;
    id?: number;
    logo_url: string;
    password: string;
    site_url: string;
    username: string;
}