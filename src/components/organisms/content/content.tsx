import { FC, useMemo, useRef } from "react"
import { AccountModel } from "../../../models/account.model"
import { InputDefault } from "../../atoms/input-default/input-default"
import { InputPassword } from "../../atoms/input-password/input-password"
import { ProfileImage } from "../../atoms/profile-image/profile-image"
import { SubButtoms } from "../../atoms/sub-buttoms/sub-buttoms"
import './content.scss'

type TypeContent ={
    mainName? : string;
    createdAt?: Date;
    urlImage?: string;
    typeContent: 'REG' | 'EDIT';
    onSend?: ( params: AccountModel  ) => void;
    account?: AccountModel
} 

const Content: FC<TypeContent> = ({account,createdAt, urlImage,mainName, typeContent, children, onSend}) => {

    const namerRef = useRef<HTMLInputElement>(null);
    const usernameRef = useRef<HTMLInputElement>(null);
    const siteUrlRef = useRef<HTMLInputElement>(null);
    const logoUrlRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);

    function sendData() {
        const json: AccountModel = {
            account_name: namerRef.current?.value || '',
            username: usernameRef.current?.value  || '',
            password: passwordRef.current?.value || '',
            site_url: siteUrlRef.current?.value || '',
            logo_url: logoUrlRef.current?.value || '',
            created_at: String(createdAt?.getTime().toString())
        }
        onSend!(json)
    }

    useMemo(() => {
        if(account) {
            namerRef.current!.value = account?.account_name || '';
            usernameRef.current!.value = account?.username || '';
            passwordRef.current!.value = account?.password || '';
            siteUrlRef.current!.value = account?.site_url || '';
            logoUrlRef.current!.value = account?.logo_url || '';

        } else {
            namerRef.current?.setAttribute('value', '');
        }
    }, [account])

    return (
        <div className="o-content">
            { typeContent === 'EDIT' &&
                <div className="o-content__header">
                    <ProfileImage urlImage={urlImage!} />
                    <h2>{mainName}</h2>
                </div>
            }
            <div className="o-content__form">
                <form action="" method="post" className="form" >
                    <div className="form__field">
                        <label>Name</label>
                        <InputDefault name="name" refName={namerRef} required />
                    </div>
                    <div className="form__field">
                        <label>Username</label>
                        <InputDefault name="username" refName={usernameRef} required />
                    </div>
                    <div className="form__field password">
                        <label>Password</label>
                        <div>
                            <InputPassword  name="password" refName={passwordRef} required />
                            <div>
                                <SubButtoms onClick={() => null} tittle="Generate"/>
                                <SubButtoms onClick={() => null} tittle="Copy"/>
                            </div>
                        </div>     
                    </div>
                    <div className="form__field site-url">
                        <label>Site Url</label>
                        <InputDefault  name="site-url" refName={siteUrlRef} required />
                    </div>
                    <div className="form__field logo-url">
                        <label>Logo Url</label>
                        <InputDefault name="logo-url" refName={logoUrlRef} required  />
                    </div>
                    <div className="form__field createdAt">
                        <label>Created At</label>
                        <p>{createdAt?.toDateString()}</p>
                    </div>
                    <div className="form__field" onClick={onSend ? sendData : undefined} >
                        {children}
                    </div>
                    
                </form>
            </div>

            
        </div>
    )
}

export {Content};
