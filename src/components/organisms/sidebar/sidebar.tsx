import { FC } from "react";
import { AccountModel } from "../../../models/account.model";
import { DataCard } from "../../molecules/data-card/data-card"
import { HeaderSidebar } from "../../molecules/header-sidebar/header-sidebar";
import './sidebar.scss';

type Props = {
    acounts: AccountModel[];
    addAction?: () => void;
    selectAccount?: (id: number) => void;
    sortAsc?: () => void;
    sortDesc?: () => void;
}


const Sidebar: FC<Props> = ({addAction, acounts, selectAccount, sortAsc, sortDesc }) => {

    return(
        <div className="o-sidebar">
            <div className="o-sidebar__header">
                <HeaderSidebar text="Sort by date:" addAction={addAction} sortAsc={sortAsc} sortDesc={sortDesc}   />
            </div>
            <div className="o-sidebar__content"  >
                { 
                    acounts.map( (ac, key) =>  <DataCard 
                        key={key}
                        onClick={selectAccount ? (id:number) => selectAccount(id) : undefined } 
                        urlImage={ac.logo_url}
                        idAccount={ac.id}
                        tittle={ac.account_name}
                        username={ac.username}
                        /> 
                    )
                }
            </div>
        </div>
    )
}


export  {Sidebar};
