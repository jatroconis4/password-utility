import { FC } from 'react'
import { Types } from '../../atoms/sub-buttoms/sub-buttoms';
import { ProfileImage, URLImage } from '../../atoms/profile-image/profile-image';
import './data-card.scss'

type TypeData = {
    idAccount?: number;
    username: string;
}


const DataCard: FC<Types & URLImage & TypeData> = ({idAccount, tittle, onClick, urlImage, username}) => {
    

    return (
        <div className="m-data-card" onClick={onClick ? () => onClick(idAccount) : undefined}>
            <div className="m-data-card__image">
                <ProfileImage urlImage={urlImage} />
            </div>
            <div className="m-data-card__content">
                <div className="m-data-card__content__acount-name">
                    {tittle}
                </div>
                <div className="m-data-card__content__username">
                    {username}
                </div>
            </div>
        </div>
    )
}

export {DataCard}