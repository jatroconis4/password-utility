import { FC } from "react";
import { ICON_ADD } from "../../../utilities/icons";
import { IconButtom } from "../../atoms/icon-buttom/icon-buttom";
import { InputMain } from "../../atoms/input-main/input-main";

import './add-search-entry.scss'

export type AddSearchEntryProps = {
    addAction?: React.MouseEventHandler<HTMLDivElement>
}

const AddSearchEntry: FC<AddSearchEntryProps> = ({addAction}) => {
    return (
        <div className="m-add-search-entry">
            <div className="m-add-search-entry__input">
                <InputMain/>
            </div>
            <div className="m-add-search-entry__add-buttom"  onClick={addAction} >
                <IconButtom icon={ICON_ADD} />
            </div>
        </div>
    )
}

export {AddSearchEntry} 
