import { FC } from 'react'
import { SubButtoms } from '../../atoms/sub-buttoms/sub-buttoms'
import { AddSearchEntry, AddSearchEntryProps } from '../add-search-entry/add-search-entry'

import './header-sidebar.scss'

type Props = {
    text: string;
    sortAsc?: () => void;
    sortDesc?: () => void;
}

export type HSidebarProps = Props & AddSearchEntryProps

const HeaderSidebar: FC<HSidebarProps> = ({text, addAction, sortAsc, sortDesc}) => {
    return (
        <div className="m-header-sidebar">
            <div className="m-header-sidebar__input">
                <AddSearchEntry addAction={addAction} /> 
            </div>
            <div className="m-header-sidebar__content">
                <div className="m-header-sidebar__content__label">
                    {text}
                </div>
                <div className="m-header-sidebar__content__buttons">
                    <SubButtoms tittle={"Asc"} onClick={sortAsc} />
                    <SubButtoms tittle={"Desc"} onClick={sortDesc} />
                </div>
            </div>
        </div>
    )
}

export {HeaderSidebar}
