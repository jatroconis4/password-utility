import { FC, useState } from 'react';
import { ICON_WATCH } from '../../../utilities/icons';
import { InputDefaultProps } from '../input-default/input-default';
import './input-password.scss'



const InputPassword: FC<InputDefaultProps>  = ({name, refName}) => {

    const showPassword = () => {
        settypeInput( typeInput === "password" ? "text" : "password" );
    }

    const [typeInput, settypeInput] = useState("password");

    return (
        <div className="a-input-password">
                <input type={typeInput} name={name} className="a-input-password__input"  ref={refName} />
            <div className="a-input-password__search-image" >
                <img src={ICON_WATCH} alt="watch"   onClick={showPassword} />
            </div>
        </div>
    )
}

export {InputPassword}
