import { FC } from 'react'
import './input-default.scss'

export type InputDefaultProps = {
    name: string;
    refName?: React.RefObject<HTMLInputElement>;
    required?: boolean;
}

const InputDefault: FC<InputDefaultProps> = ({name, refName, required}) => {
    return (
        <div className="a-input-default">
            <input required ref={refName} name={name} type="text" className="a-input-default__input" />
        </div>
    )
}

export {InputDefault}
