import { FC } from 'react'
import './profile-image.scss'

export type URLImage = {
    urlImage: string
}


const ProfileImage : FC<URLImage>  = ({urlImage}) => {
    return (
        <div className="a-profile-image">
            <img src={urlImage} alt="Profile" />
        </div>
    )
}

export {ProfileImage}
 