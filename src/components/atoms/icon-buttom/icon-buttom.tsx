import { FC } from 'react';
import './icon-buttom.scss';

type Props = {
    icon: string;
    label?: string;
}

const IconButtom: FC<Props> = ({icon, label}) => {
    return (
        <div className={"a-add-buttom"}  >
            <img src={icon} alt="add-icon"/>
            {label && <span>{label}</span> }
        </div>
    )
}

export {IconButtom};
