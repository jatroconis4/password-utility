import { ICON_SEARCH } from '../../../utilities/icons';
import './input-main.scss';

const InputMain = () => {
    return (
        <div className="a-input-main">
            <div className="a-input-main__search-image">
                <img src={ICON_SEARCH} alt="search" />
            </div>
            <input type="text" className="a-input-main__input"  placeholder="Search" />
        </div>
    )
}

export {InputMain}; 

