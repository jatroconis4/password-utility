import { FC, SyntheticEvent } from 'react';
import './sub-buttoms.scss'

export type Types = {
    tittle?: string;
    onClick?: (e: any)  => void;
}

const SubButtoms : FC<Types> = ({tittle , onClick}) => {
    return (
        <div className="a-sub-buttoms" onClick={onClick} >
            <div className="a-sub-buttoms__tittle">
                {tittle}
            </div>
        </div>
    )
}

export  {SubButtoms};
