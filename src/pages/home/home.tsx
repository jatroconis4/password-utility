import axios from "axios";
import { useEffect, useState } from "react";
import { IconButtom } from "../../components/atoms/icon-buttom/icon-buttom";
import { Content } from "../../components/organisms/content/content";
import { Sidebar } from "../../components/organisms/sidebar/sidebar";
import { AccountModel } from "../../models/account.model";
import { ICON_ADD } from "../../utilities/icons";

import './home.scss'
const Home = () => {

    const urlimage = "https://images.clarin.com/2016/04/05/HJxaM4fy4g_340x340.jpg";
    const cssClass = 'p-home';

    async function listarData(){
        try{
            const response = await axios.get<AccountModel[]>('http://localhost:3000/passwords');
            if(response.status === 200 ){
                const data = response.data;
                console.log(data);
                setAccounts(data);
            }
        }catch (e){
            console.log(e)
        }
    }

    async function registerData(data: AccountModel){
        const response = await axios.post<AccountModel>('http://localhost:3000/passwords', data);
        if(response.status === 201) {
            listarData();
        }
    }

    async function getData(id:number){
        const response = await axios.get<AccountModel>(`http://localhost:3000/passwords/${id}`);
        if(response.status === 200 ) {
            setAccountedit(response.data);
        }
    }

    useEffect(() => {
        listarData();
    }, [])

    const [accountEdit, setAccountedit] = useState<AccountModel>();
    const [acounts, setAccounts] = useState<AccountModel[]>([]);
    const [showModal, setShowModal] = useState(false);
    const toggleModal = () => setShowModal(!showModal) 

    function getAccount( account: AccountModel ) {
        registerData(account);
        toggleModal();
    }

    function sortDesc() {
        const newAcc = acounts.sort((a, b) => (a.id! > b.id! ? -1 : 1))
        setAccounts([...newAcc]);
    }

    function sortAsc() {
        const newAcc = acounts.sort((a, b) => (a.id! > b.id! ? 1 : -1))
        setAccounts([...newAcc]);
    }

    return (
        <div className={cssClass}  >   
            <div className={showModal ? `${cssClass}__sidebar --show-modal`  : `${cssClass}__sidebar`} 
                onClick={showModal ? toggleModal: undefined}  
            >
                <Sidebar acounts={acounts} sortDesc={sortDesc} sortAsc={sortAsc} addAction={toggleModal} selectAccount={getData} />
            </div>
            <div className={`${cssClass}__content`} >
                <Content urlImage={urlimage} mainName="Account Name" typeContent={"EDIT"} account={accountEdit} />
            </div>
            { showModal && 
                <div className={`${cssClass}__modals`} >
                    <Content  createdAt={new Date()} urlImage={urlimage} mainName="Account Name" typeContent={"REG"} onSend={getAccount} >
                        <IconButtom icon={ICON_ADD} label="Registrar"  />
                    </Content>
                </div>
            }
            
        </div>
    )
}

export  {Home};
