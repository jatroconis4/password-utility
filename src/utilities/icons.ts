import ICON_ADD from '../assets/svg/main-add.svg';
import ICON_SEARCH from '../assets/svg/search.svg';
import ICON_WATCH from '../assets/svg/watch.svg';



export {
    ICON_ADD,
    ICON_SEARCH,
    ICON_WATCH
}